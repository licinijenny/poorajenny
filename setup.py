from setuptools import setup

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setup(name='PooraJenny2',
      version='0.2',
      description='Poorra apuestas',
      url='https://gitlab.com/jennylicini/poorajenny.git',
      author='Jenny Licini',
      author_email='y5762434m@iespoblenou.org',
      long_description=long_description,
      long_description_content_type="text/markdown",
      license='MIT',
      packages=['poorajenny'],
      classifiers=[
          "Programming Language :: Python :: 3",
          "License :: OSI Approved :: MIT License",
          "Operating System :: OS Independent",
      ],
      zip_safe=False)
